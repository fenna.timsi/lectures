# Solutions for LCAO model exercises

### Exercise 1

#### Question 1.
See lecture notes.
#### Question 2.
The atomic number of Tungsten is 74:

$$
1s^22s^22p^63s^23p^64s^23d^{10}4p^65s^24d^{10}5p^66s^24f^{14}5d^4
$$

#### Question 3.

\begin{align}
\textrm{Cu} &= [\textrm{Ar}]4s^23d^9\\
\textrm{Pd} &= [\textrm{Kr}]5s^24d^8\\
\textrm{Ag} &= [\textrm{Kr}]5s^24d^9\\
\textrm{Au} &= [\textrm{Xe}]6s^24f^{14}5d^9
\end{align}

### Exercise 2
#### Question 1.

$$
\psi(x) =
\begin{cases}
    &\sqrt{κ}e^{κ(x-x_1)}, x<x_1\\
    &\sqrt{κ}e^{-κ(x-x_1)}, x>x_1
\end{cases}
$$

Where $κ = \sqrt{\frac{-2mE}{\hbar^2}} = \frac{mV_0}{\hbar^2}$.
The energy is given by $ϵ_1 = ϵ_2 = -\frac{mV_0^2}{2\hbar^2}$
The wave function of a single delta peak is given by

$$
    \psi_1(x) = \frac{\sqrt{mV_0}}{\hbar}e^{-\frac{mV_0}{\hbar^2}|x-x_1|}
$$

$\psi_2(x)$ can be found by replacing $x_1$ by $x_2$

#### Question 2.

$$
H = -\frac{mV_0^2}{\hbar^2}\begin{pmatrix}
    1/2+\exp(-\frac{2mV_0}{\hbar^2}|x_2-x_1|) &
    \exp(-\frac{mV_0}{\hbar^2}|x_2-x_1|)\\
    \exp(-\frac{mV_0}{\hbar^2}|x_2-x_1|) &
    1/2+\exp(-\frac{2mV_0}{\hbar^2}|x_2-x_1|)
\end{pmatrix}
$$

#### Question 3.

$$
ϵ_{\pm} = \beta(1/2+\exp(-2\alpha) \pm \exp(-\alpha))
$$

Where $\beta = -\frac{mV_0^2}{\hbar^2}$ and $α = \frac{mV_0}{\hbar^2}|x_2-x_1|$

### Exercise 3

#### Question 1.

$$
    H_{\mathcal{E}} = ex\mathcal{E},
$$

#### Question 2.

$$
    \hat{H} = \begin{pmatrix}
    E_0  & -t\\
    -t & E_0 
    \end{pmatrix} +\begin{pmatrix}
    ⟨1|ex\mathcal{E}|1⟩ & ⟨1|ex\mathcal{E}|2⟩\\
    ⟨2|ex\mathcal{E}|1⟩ & ⟨2|ex\mathcal{E}|2⟩
    \end{pmatrix} = \begin{pmatrix}
    E_0 - \gamma & -t\\
    -t & E_0 + \gamma
    \end{pmatrix},
$$
where $\gamma = e d \mathcal{E}/2$ and have used $$⟨1|ex\mathcal{E}|1⟩ = -e d \mathcal{E}/2⟨1|1⟩ = -e d \mathcal{E}/2$$

#### Question 3.

The eigenstates of the Hamiltonian are given by:
$$
    E_{\pm} = E_0\pm\sqrt{t^2+\gamma^2}
$$
The ground state wave function is:
$$
    \begin{split}
        |\psi⟩ &= \frac{t}{\sqrt{(\gamma+\sqrt{\gamma^2+t^2})^2+t^2}}\begin{pmatrix}
        \frac{\gamma+\sqrt{t^2+\gamma^2}}{t}\\
        1
        \end{pmatrix}\\
        |\psi⟩ &= \frac{\gamma+\sqrt{t^2+\gamma^2}}{\sqrt{(\gamma+\sqrt{\gamma^2+t^2})^2+t^2}}|1⟩+\frac{t}{\sqrt{(\gamma+\sqrt{\gamma^2+t^2})^2+t^2}}|2⟩
    \end{split}
$$

#### Question 4.

$$
    P = -\frac{2\gamma^2}{\mathcal{E}}(\frac{1}{\sqrt{\gamma^2+t^2}})
$$
