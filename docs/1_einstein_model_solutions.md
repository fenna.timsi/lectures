---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 0.8.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["initialize"]
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Solutions for lecture 1 exercises
### Warm-up exercises
#### Question 1.
An ideal gas only contains 3 momentum degrees of freedom.
#### Question 2.
$C = 2k_B$.
#### Question 3.
See image below (with $T_1 < T_2$)
#### Question 4.
Minus sign in the exponent. This would result in $n_B(T = 0) = -1$, which is not physical.
#### Question 5.
See plot with slider

```python
fig, ax = plt.subplots()
omega = np.linspace(0.1, 3)
T = [1,2]
ax.plot(omega, 1/(np.exp(omega/T[0]) - 1), label = r'$T_1$')
ax.plot(omega, 1/(np.exp(omega/T[1]) - 1), label = r'$T_2$')
ax.set_ylim([0,3])
ax.set_xlim([0,3])
ax.set_xlabel('$\hbar \omega$')
ax.set_xticks([0])
ax.set_xticklabels(['$0$'])
ax.set_ylabel('$n_B$')
ax.set_yticks([0,1, 2])
ax.set_yticklabels(['$0$','$1$', '$2$'])
ax.legend()
draw_classic_axes(ax, xlabeloffset=.2)
```

### Exercise 1: Heat capacity of a classical oscillator.

#### Question 1.
The answer is

$$
Z = \int_{-\infty}^{\infty}dp \int_{-\infty}^{\infty} dx e^{-\frac{\beta}{2m}p^2-\frac{\beta k}{2}x^2} = \sqrt{\frac{2\pi m}{\beta}}\sqrt{\frac{2\pi}{\beta k}} = \frac{2\pi}{\beta}\sqrt{\frac{m}{k}},
$$

where we used $\int_{-\infty}^{\infty}e^{-\alpha x^2} = \sqrt{\frac{\pi}{\alpha}}$.

#### Question 2.

$$
\langle E \rangle = -\frac{1}{Z}\frac{\partial Z}{\partial \beta} = \frac{1}{\beta}
$$

#### Question 3.

$$
C = \frac{\partial\langle E\rangle}{\partial T} = k_B
$$

The heat capacity is temperature independent.

### Exercise 2: Quantum harmonic oscillator.

#### Question 1.

$$
Z = \sum_{n = 0}^{\infty} e^{-\beta\hbar\omega(n + 1/2)} = e^{-\beta\hbar\omega/2}\frac{1}{1 - e^{-\beta\hbar\omega}} = \frac{1}{2\sinh(\beta\hbar\omega/2)},
$$

where we used $\sum_{n = 0}^{\infty}r^n = \frac{1}{1 - r}$.

#### Question 2.

$$
\langle E\rangle = -\frac{1}{Z}\frac{\partial Z}{\partial\beta} = \frac{\hbar\omega}{2}\coth\frac{\beta\hbar\omega}{2} = \hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right) = \hbar\omega\left(n_B(\beta\hbar\omega) + \frac{1}{2}\right).
$$

#### Question 3.

$$
C = \frac{\partial \langle E\rangle}{\partial T} = \frac{\partial\langle E\rangle}{\partial\beta}\frac{\partial\beta}{\partial T} = k_B(\beta\hbar\omega)^2\frac{e^{\beta\hbar\omega}}{(e^{\beta\hbar\omega} - 1)^2}.
$$

In the high temperature limit $\beta \rightarrow 0$ and $e^{\beta\hbar\omega} \approx 1 + \beta\hbar\omega$, so $C \rightarrow k_B$ which is the same result as in Exercise 1.3.

#### Question 4.
Compare your result with the plot with the slider. 
Did you correctly indicate the where the Einstein temperature is?

#### Question 5.

$$
\langle n\rangle = \frac{1}{Z}\sum_{n = 0}^{\infty} ne^{-\beta\hbar\omega(n + 1/2)} = 2\frac{e^{\beta\hbar\omega/2} - e^{-\beta\hbar\omega/2}}{2}e^{-\beta\hbar\omega/2}\frac{e^{-\beta\hbar\omega}}{(1 - e^{-\beta\hbar\omega})^2} = \frac{1}{e^{\beta\hbar\omega} - 1},
$$

where we used $\sum_{n = 0}^{\infty}nr^n = \frac{r}{(1 - r)^2}$.

### Exercise 3: Total heat capacity of a diatomic material.

#### Question 1.
Use the formula $\omega = \sqrt{\frac{k}{m}}$.

#### Question 2.
Energy per atom is given by 

$$
E = \frac{N_{^6Li}}{N}\hbar\omega_{^6Li}(2 + 1/2) + \frac{N_{^7Li}}{N}\hbar\omega_{^7Li}(4 + 1/2).
$$

#### Question 3.
Energy per atom is given by

$$
E = \frac{N_{^6Li}}{N}\hbar\omega_{^6Li}\left(n_B(\beta\hbar\omega_{^6Li}) + \frac{1}{2}\right) + \frac{N_{^7Li}}{N}\hbar\omega_{^7Li}\left(n_B(\beta\hbar\omega_{^7Li}) + \frac{1}{2}\right).
$$

#### Question 4.
Heat capacity per atom is given by

$$
C = \frac{N_{^6Li}}{N}C_{^6Li} + \frac{N_{^7Li}}{N}C_{^7Li},
$$

where the heat capacities are calculated with the formula from Excercise 2.4.
